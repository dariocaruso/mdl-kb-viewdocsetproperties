﻿'use strict';

function initialize(domContainer) {

    var fields_standard = {
        Title: "Title",
        KBTags: "Tags",
        DescrizioneStepProcesso: "Descrizione Step Processo",
        InVigore: "In Vigore",
        Content_x005f_x0020_x005f_Owner: "Content Owner"
    };

    var fields_schedaVideo = {
        Title: "Title",
        Riferimento_x005f_x0020_x005f_al_x005f_x0020_x005f_modulo_x005f_x0020_x005f_formazione: "Categoria di riferimento",
        KBTags: "Tags",
        Content_x005f_x0020_x005f_Owner: "Content Owner",
        Data_x005f_x0020_x005f_di_x005f_x0020_x005f_pubblicazione: "Data di pubblicazione",
        Elenco_x005f_x0020_x005f_argomenti_x005f_x0020_x005f_trattati: "Elenco argomenti trattati",
        Key_x005f_x0020_x005f_Points: "Key Points",
        N_x005f_x00B0_x005f__x005f_x0020_x005f_puntata: "N° puntata",
        Sintesi_x005f_x002F_x005f_descrizione_x005f_x0020_x005f_per_x005f_x0020_x005f_singolo_x005f_x0020_x005f_argomento: "Sintesi/descrizione per singolo argomento"
    }

    var fields_operatività = {
        Title: "Title",
        Riferimento_x005f_x0020_x005f_al_x005f_x0020_x005f_modulo_x005f_x0020_x005f_formazione: "Categoria di riferimento",
        KBTags: "Tags",
        Content_x005f_x0020_x005f_Owner: "Content Owner",
        DataFine: "Data fine",
        DataInizio: "Data inizio",
        Descrizione_x005f_x0020_x005f_di_x005f_x0020_x005f_dettaglio_x005f_x0020_x005f_step_x005f_x0020_x005f_di_x005f_x0020_x005f_processo: "Descrizione di dettaglio step di processo",
        Fino_x005f_x0020_x005f_alla_x005f_x0020_x005f_revoca: "Fino alla revoca",
        In_x005f_x0020_x005f_vigore: "In vigore",
        Scadenza_x005f_x0020_x005f_contenuto: "Scadenza contenuto",
        Schermate_x005f_x0020_x005f_transazioni: "Schermate transazioni",
        Struttura_x005f_x0020_x005f_che_x005f_x0020_x005f_applica_x005f_x0020_x005f_l_x005f_x0027_x005f_operativit_x005f_x00e0_x005f__x005f_x0020_x005f_descritta: "Struttura che applica l'operatività descritta",
        Tempistiche_x005f_x0020_x005f_di_x005f_x0020_x005f_lavorazione_x005f_x0020_x005f__x005f_x0028_x005f_SLA_x005f_x0029_x005f_: "Tempistiche di lavorazione (SLA)"
    }

    DS_HL_INT.getDocumentSet(
        function (data) {
                       
            var fields = getTemplateByContentType(contentTypeName);
            // success callback 
            var jsonObject = JSON.parse(data.body);
            var results = jsonObject.d;   
            var index;

            for (var field in fields) {
                for (var key in results) {
                    if (key === field) {
                        if (typeof results[key] !== "undefined" && results[key] !== null) {
                            if (!isEmpty(results[key]))
                            {
                                $(domContainer).append("<h3>" + fields[field] + "</h3><div>" + results[key] + "</div>");
                                break;
                            }                                
                        }
                    }
                }
            }

            $("#container").accordion({
                collapsible: true,
                heightStyle: "content"
            });            
        },
        function (data, errorCode, errorMessage) {                          // error callback
            var documentSetHTML = DS_HL_RDR.parseError(errorMessage, true);
            $(domContainer).html(documentSetHTML);
        }
    );

    function getTemplateByContentType(contentTypeName) {
        switch (contentTypeName) {
            case "Scheda Video":
                return fields_schedaVideo;
                break;
            case "Operatività":
                return fields_operatività;
                break;
            case "Set di documenti":
                return fields_operatività;
                break;
            default:
                return fields_standard;
        }
    }

    function isEmpty(str) {
        return (!str || 0 === str.length);
    }
}
