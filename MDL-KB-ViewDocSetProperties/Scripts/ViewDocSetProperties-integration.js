﻿'use strict';

var contentTypeName;

var DS_HL_INT = (function () {

    // private variables and methods
    var DOCUMENT_SET_URL_TEMPLATE = "{APP_WEB_URL}/_api/SP.AppContextSite(@target)/web/lists/getbyid('{LIST_GUID}')/items({ITEM_ID})/FieldValuesAsHtml?@target='{HOST_WEB_URL}'";
   
    // context variables
    var hostWebUrl;
    var appWebUrl;
   
    // document set identifiers
    var listId;
    var itemId;

    // callback functions
    var currentCallback;
    var currentOnSuccess;
    var currentOnError;

    //This function is used to get query string parameters  
    function manageQueryStringParameter(paramToRetrieve) {
        var params =
        document.URL.split("?")[1].split("&");
        var strParams = "";
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split("=");
            if (singleParam[0] == paramToRetrieve) {
                return singleParam[1];
            }
        }
    };   

    function getQueryStringParameter(key, urlToParse) {
        if (!urlToParse || urlToParse.length === 0) {
            urlToParse = document.URL;
        }
        if (urlToParse.indexOf("?") === -1) {
            return "";
        }
        var params = urlToParse.split('?')[1].split('&');
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split('=');
            if (singleParam[0] === key) {
                return decodeURIComponent(singleParam[1]);
            }
        }
        return "";
    }

    function getHostPageInfoListener (e) {
        // <summary>Callback function for getting the host page's info via postMessage api.</summary>
        console.log("postMessage response received: " + e.data);

        var messageData;
        try {
            messageData = JSON.parse(e.data);
        }
        catch (error) {
            console.log("Unable to parse the response from the host page.");
            return;
        }

        var requestUrl = "";
        var requestData = {};

        // Store Content Type Name
        var url = messageData._spPageContextInfo.serverRequestPath;
        var res = url.split("/");
        contentTypeName = res[res.length - 2];

        listId = getQueryStringParameter("List", messageData.hostPageURL);
        if (!listId || listId.length === 0) {
            console.log("Could not find an listGUID querystring parameter.");
        }

        itemId = getQueryStringParameter("ID", messageData.hostPageURL);
        if (!itemId || itemId.length === 0) {
            console.log("Could not find an ID querystring parameter.");
        }

        //callback function
        if (typeof currentCallback == 'function') {
            currentCallback(currentOnSuccess, currentOnError);
        }
    };

    function getDocumentSetCallbackFunction(onSuccess, onError) {

        var url = DOCUMENT_SET_URL_TEMPLATE
                .replace('{APP_WEB_URL}', appWebUrl)
                .replace('{LIST_GUID}', listId)
                .replace('{ITEM_ID}', itemId)
                .replace('{HOST_WEB_URL}', hostWebUrl);

        var executor = new SP.RequestExecutor(appWebUrl);
        executor.executeAsync({
            url: url,
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: onSuccess,
            error: onError
        }
        );
    };

    // module's public api
    return {

        /*
         * Retrieves the current document set

         * param: onSuccess - the function to call in case of success
         * param: onError - the function to call in case of error
         */
        getDocumentSet: function (onSuccess, onError) {

            // retrieve context variables
            hostWebUrl = decodeURIComponent(manageQueryStringParameter('SPHostUrl'));
            appWebUrl = decodeURIComponent(manageQueryStringParameter('SPAppWebUrl'));

            // Register the listener
            if (typeof window.addEventListener !== 'undefined') {
                window.addEventListener('message', getHostPageInfoListener, false);
            }
            else if (typeof window.attachEvent !== 'undefined') {
                window.attachEvent('onmessage', getHostPageInfoListener);
            }

            // Set callback function
            currentCallback = getDocumentSetCallbackFunction;
            currentOnSuccess = onSuccess;
            currentOnError = onError;

            // Send the host page a message
            var hostPageMessage = {};
            hostPageMessage.message = "getHostPageInfo";
            var hostPageMessageString = JSON.stringify(hostPageMessage);
            window.parent.postMessage(hostPageMessageString, document.referrer);
            console.log("Sent host page a message: " + hostPageMessageString);
        }
    }
}());
