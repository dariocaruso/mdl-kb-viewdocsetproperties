﻿'use strict';

/*
 * Module docsethighlights-renderer.
 * Exposes methods to render document set highlights 
 * in a predefined HTML format.
 */
var DS_HL_RDR = (function () {

    var DOCUMENT_SET_TEMPLATE_OPERATIVITA =
    "<div class='docset-properties'> \
        <h3> {Title} </h3> <br/><br/> \
        <br/> \
        Categoria di riferimento: {Categoria-di-riferimento} <br/><br/> \
        Tags: {Tags} <br/> \
        Content Owner: {Content-OwnerId} <br/><br/> \
        Data fine: {data-fine} <br/> \
        Data inizio: {data-inizio} <br/> \
        Descrizione di dettaglio step di processo: {Descrizione-di-dettaglio-step-di-processo} <br/> \
        Fino alla revoca: {fino-alla-revoca} <br/> \
        In vigore: {in-vigore} <br/> \
        Scadenza contenuto: {scadenza-contenuto} <br/> \
        Schermate transazioni: {schermate-transazioni}<br/> \
        Struttura che applica l'operatività descritta: {Struttura-che-applica-operatività-descritta} <br/> \
        Tempistiche di lavorazione (SLA): {Tempistiche-di-lavorazione} <br/> \
        \
     </div>";

    var DOCUMENT_SET_TEMPLATE_SCHEDE_VIDEO  =
    "<div class='docset-properties'> \
        <h3> {Title} </h3> <br/><br/> \
        <br/> \
        Categoria di riferimento: {Categoria-di-riferimento} <br/><br/> \
        Tags: {Tags} <br/> \
        Content Owner: {Content-OwnerId} <br/><br/> \
        Data di pubblicazione : {data-pubblicazione} <br/> \
        Elenco argomenti trattati: {elenco-argomenti-trattati} <br/> \
        Key Points: {key-point} <br/> \
        N° Puntata: {n-puntata} <br/> \
        Scadenza contenuto: {scadenza-contenuto} <br/> \
        Schermate transazioni: {schermate-transazioni}<br/> \
     </div>";   

    var ERROR_TEMPLATE =
    "<div class='error'> \
        <span class='error-message'>{error_message}</span> \
    </div>";

    //public API
    return {
        /*
         * Renders document set highlights
         */
        parseDocumentSetHighlights: function (data) {
            
            var documentSetHighlightsHTML = 'No data found.';

            if (!(typeof data === 'undefined')) {
                documentSetHighlightsHTML = DOCUMENT_SET_HIGHLIGHTS_TEMPLATE
                .replace('{Title}', data.d.Title)
                .replace('{Categoria-di-riferimento}', data.d.Modified)
                .replace('{in-vigore}', data.d.In_x0020_vigore)
                .replace('{data-inizio}', data.d.StartDate)
                .replace('{data-fine}', data.d.OData__EndDate)
                .replace('{content-owner}', data.d.Content_x0020_OwnerId)
                .replace('{Tags}', data.d.Content_x0020_OwnerId)
                .replace('{Descrizione-di-dettaglio-step-di-processo}', data.d.Content_x0020_OwnerId)
                .replace('{fino-alla-revoca}', data.d.Content_x0020_OwnerId)
                .replace('{scadenza-contenuto}', data.d.Content_x0020_OwnerId)
                .replace('{schermate-transazion}', data.d.Content_x0020_OwnerId)
                .replace('{Struttura-che-applica-operatività-descritta}', data.d.Content_x0020_OwnerId)
                .replace('{Tempistiche-di-lavorazione}', data.d.Content_x0020_OwnerId);
            }

            return documentSetHighlightsHTML;
        },

        /*
         * Renders an error.
         * if includeDetails is set to false, does not include error details, and show a generic error message.
         * @param includeDetails: if false include error details (defaults to true).
         * @param error: error detail.
         */
        parseError: function (error, includeDetails) {
            if (includeDetails === 'undefined') {
                includeDetails = true;
            }
            var errorMessage = includeDetails ? 'An error occured: ' + error : 'An error occured.';
            return ERROR_TEMPLATE.replace('{error_message}', errorMessage);
        }
    }
}());